import nodeResolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';
//import peerDepsExternal from "rollup-plugin-peer-deps-external";
import babel from '@rollup/plugin-babel';
import replace from '@rollup/plugin-replace';
import css from "rollup-plugin-import-css";
import packageJson from "./package.json";


export default {
  input: "src/index.js",
  output: [
    {
      file: packageJson.main,
      format: "cjs",
      sourcemap: true,
    },
    {
      file: packageJson.module,
      format: "esm",
      sourcemap: true,
    },
    {
      file: packageJson.universal,
      format: "umd",
      sourcemap: true,
      name: "OpenApiBrowser",
      globals: {
        react: 'React',
        'react-dom': 'ReactDOM'
      }
    },
  ],
  plugins: [
    //peerDepsExternal(),
    nodeResolve({
      extensions: ['.js', '.jsx']
    }),
    css({
      output: 'assets/styles.css'
    }),
    babel({
      babelHelpers: 'bundled',
      presets: ['@babel/preset-react'],
      extensions: ['.js', '.jsx']
    }),
    commonjs(),
    replace({
      preventAssignment: false,
      'process.env.NODE_ENV': '"development"'
    }),
    terser(),
  ],
  external: ["react", "react-dom"],
};
