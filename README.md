# OpenAPI Browser
> A lightweight JS browser for multiple OpenAPI version browsing

This repository skeleton has not been designed as a React application or React components.
React is used under the hood but the goal is to produce a JS library in UMD format (Universal Module Definition) to be imported as vanilla JS in browwer.
```html
<html>
<head>
    <!-- react and reactDOM are peer dependency, and must be imported manually -->
    <script crossorigin src="https://unpkg.com/react@18/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@18/umd/react-dom.development.js"></script>
    <!-- library import -->
    <script charset="UTF-8" type="text/javascript" src="https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fopenapi-browser/packages/generic/openapi-browser-landscape-viewer/latest/index.js"></script>
</head>
<body>
<div id="root"></div>
<script>
  // Vanilla JS library usage
  OpenApiBrowser({
    rootElementId: 'root',
    props: {}
  })
</script>
</body>
</html>
```

## Contributiong

```shell
npm install
npm run storybook
# or
npm run demo
```

### Test
```
npm run build
```
and open test/browser/index.htm in web browser
