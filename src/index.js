import React from 'react'
import ReactDOM from "react-dom/client";
import {Browser} from "./components";

import * as components from './components'

function OpenApiBrowser(args) {
  const {rootElementId = "root", props ={}} = args || {}
  const root = ReactDOM.createRoot(document.getElementById(rootElementId));
  const browser = React.createElement(Browser, props);
  root.render(browser);
}

OpenApiBrowser.components = components

export default OpenApiBrowser
