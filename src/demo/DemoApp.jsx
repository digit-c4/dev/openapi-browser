import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './DemoApp.css'
import Browser from '../components/Browser'

function DemoApp() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>

      <p className="read-the-docs">
        Demonstration of your nice components in a dummy application
      </p>
      <div style={{border: 'solid 1px black'}}>
        <Browser versions={{
            'openapi: 3.0.2': 'https://raw.githubusercontent.com/swagger-api/swagger-petstore/master/src/main/resources/openapi.yaml',
            'openapi: 3.1.0': 'https://raw.githubusercontent.com/swagger-api/swagger-petstore/v31/src/main/webapp/code-first/openapi.yaml',
        }} />
      </div>
    </>
  )
}

export default DemoApp
