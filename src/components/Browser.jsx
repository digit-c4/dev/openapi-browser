import React from 'react';
import PropTypes from 'prop-types';
import VersionSelector from './VersionSelector';

export default function Browser({ versions }) {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <VersionSelector versions={versions} /> {/* Pass the versions prop */}
    </div>
  );
}

Browser.propTypes = {
  versions: PropTypes.object,
};
Browser.defaultProps = {
  versions: {},
};