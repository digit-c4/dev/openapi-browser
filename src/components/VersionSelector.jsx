import React, { useState, useEffect } from 'react';
import SwaggerUI from 'swagger-ui';
import 'swagger-ui/dist/swagger-ui.css';

const SwaggerUiComponent = ({ url }) => {
  const swagger = React.useRef();

  React.useEffect(() => {
    swagger.current = SwaggerUI({
      dom_id: '#blabetiblou',
      url,
    });
  }, [url]);

  React.useEffect(() => {
    if (!swagger.current) return;
    swagger.current.specActions.updateUrl(url);
    // trigger remote definition fetch
    swagger.current.specActions.download(url);
  }, [url]);

  return (
    <div id="blabetiblou" />
  );
};

const VersionSelector = ({ versions }) => {
  const [selectedVersion, setSelectedVersion] = useState(null);

  useEffect(() => {
    // Select the first version by default
    const firstVersion = Object.keys(versions)[0];
    setSelectedVersion(firstVersion);
  }, [versions]);

  const handleVersionSelect = (version) => {
    setSelectedVersion(version);
  };

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '200px', backgroundColor: 'lightgrey' }}>
        <ul style={{ listStyle: 'none' }}>
          {Object.keys(versions).map((version) => (
            <li key={version}>
              <button onClick={() => handleVersionSelect(version)}>
                {version} {/* Display the version name */}
              </button>
            </li>
          ))}
        </ul>
      </div>
      <div style={{ flex: 1 }}>
        {selectedVersion && (
          <div style={{ width: '100%' }}>
            <SwaggerUiComponent url={versions[selectedVersion]} docExpansion="list" />
          </div>
        )}
      </div>
    </div>
  );
};

export default VersionSelector;
